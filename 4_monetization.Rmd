# Метрики монетизации pt.1 {#c4_monetization}

## Запись занятия {-}

<iframe width="560" height="315" src="https://www.youtube.com/embed/_YxGw_s1PV4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Gross / Net {-}

Gross - общая сумма всех платежей

Revenue (или Net) - сумма платежей после вычета налогов и комиссии магазина приложений. 

### задача 1 {-}
Нарисовать график суммы платежей по дням

```{r, warning=FALSE, message=FALSE}
library(data.table)
library(plotly)
payments <- fread('https://gitlab.com/hse_mar/mar211f/-/raw/main/data/payments_custom.csv')
daily_gross <- payments[, list(n_payers = uniqueN(user_pseudo_id), 
                               gross = sum(gross)), by = pay_dt]
daily_gross <- daily_gross[order(pay_dt)]

plot_ly(daily_gross, x = ~pay_dt, y = ~gross, type = 'scatter', mode = 'none', stackgroup = 'one') %>%
  layout(
    # title = 'Выручка в день',
    title = 'Gross по дням'
  ) %>%
  config(displayModeBar = FALSE)

```

### задача 2 {-}

Добавить на график группировку по платформам.

```{r}
daily_gross_platform <- payments[, list(n_payers = uniqueN(user_pseudo_id), 
                               gross = sum(gross)), by = list(platform, pay_dt)]
daily_gross_platform <- daily_gross_platform[order(pay_dt)]

# area plot
plot_ly(daily_gross_platform, x = ~pay_dt, y = ~gross, color = ~platform,
        type = 'scatter', mode = 'none', stackgroup = 'one') %>%
  layout(
    # title = 'Выручка в день',
    title = 'Gross по дням, группировка по платформам'
  ) %>%
  config(displayModeBar = FALSE)
```


```{r}
# второй вариант, линиями
plot_ly(daily_gross_platform, x = ~pay_dt, y = ~gross, color = ~platform,
        type = 'scatter', mode = 'line') %>%
  layout(
    # title = 'Выручка в день',
    title = 'Gross по дням, группировка по платформам',
    yaxis = list(rangemode = 'tozero')
  ) %>%
  config(displayModeBar = FALSE)
```

### задача 3 {-}

Добавить на график группировку по offer_type 

```{r}
daily_gross_offer <- payments[, list(n_payers = uniqueN(user_pseudo_id), 
                                        gross = sum(gross)), by = list(offer_type, pay_dt)]
daily_gross_offer <- daily_gross_offer[order(offer_type, pay_dt)]

plot_ly(daily_gross_offer, x = ~pay_dt, y = ~gross, color = ~offer_type,
        type = 'scatter', mode = 'none', stackgroup = 'one') %>%
  layout(
    # title = 'Выручка в день',
    title = 'Gross по дням, группировка по типам платежей'
  ) %>%
  config(displayModeBar = FALSE)
```


<br>

## Конверсия {-}
Conversion = N Paying Users / N Users

Конверсия обычно считается в каком-то "окне", - минимальном общем количестве дней, которые могли прожить в приложении пользователи разных сегментов когорт. Например, пользователи, которые пришли месяц назад, могли платить 30 дней. Пользователи, которые пришли пять дней назад - могли платить только пять дней. Соответственно, если мы хотим сравнивать конверсию этих двух когорт, то считать надо с ограничением в пять дней - сколько пользователей первой когорты сконвертировалось за пять дней в приложении (несмотря на то, что они пришли месяц назад), и сколько пользователей второй когорты сконвертировалось за пять дней.

Аналогично, когда оцениваем конверсию месячной когорты (всех пользователей, которые, например, пришли в сентябре), то надо так же считать конверсию только за какое-то определенное количество дней, чтобы не было перекосов из-за неравномерной длительности жизни пользователей в приложении.

<br>

### задача 4 {-}

Посчитать, какая доля пользователей, которая пришла в июне, стала платящими в интервале 30 дней от инсталла.

**Алгоритм 1 (не очень гибкий):**
 - посчитать количество платящих в payments, у которых install_dt был в июне, а lifetime (дата платежа  - дата инсталла) меньше или равен 30
 - по таблице installs посчитать, сколько всего пришло пользователей в июне
 - поделить одно на другое

```{r}
# импортируем инсталлы
installs <- fread('https://gitlab.com/hse_mar/mar211f/-/raw/main/data/installs.csv')

# создаем епременную лайфтайма
payments[, lifetime := as.numeric(pay_dt - install_dt)]

# выделяем сегмент пользователей
payers_june <- payments[install_dt >= '2022-06-01' & install_dt < '2022-07-01']
payers_june <- payers_june[lifetime >= 0 & lifetime <= 30]

payers_june[, uniqueN(user_pseudo_id)] / installs[dt >= '2022-06-01' & dt < '2022-07-01', uniqueN(user_pseudo_id)]
```

**Алгоритм 2, тоже не очень гибкий**
 - посчитать минимальный лайфтайм пользователей по таблице платежей
 - приджойнить результат к таблице инсталлов
 - посчитать количество пользователей всего и количество пользователей с ненулевым лайфтаймом
 - поделить одно на другое

```{r}
payers_min <- payments[install_dt >= '2022-06-01' & install_dt < '2022-07-01']
payers_min <- payers_min[lifetime <= 30]
payers_min <- payers_min[, list(min_pay_dt = min(pay_dt)), by = user_pseudo_id]

jun_payers_min <- merge(
  installs[dt >= '2022-06-01' & dt < '2022-07-01'],
  payers_min,
  by = 'user_pseudo_id', all.x = TRUE
)

jun_payers_min_stat <- jun_payers_min[, list(
  total_users = uniqueN(user_pseudo_id), 
  payers = uniqueN(user_pseudo_id[!is.na(min_pay_dt)]))]

jun_payers_min_stat[, payers / total_users]
```

<br>

## ARPU / ARPPU {-}
Averange revenue per user - сумма платежей за определенный период, деленная на общее количество пользователей когорты. Средний чек, наверное, одна из самых важных метрик для оперирования продуктом, так как изучение структуры ARPU позволяет понять, за что платят пользователи и как можно улучшить эту метрику и так далее.

Average revenue per paying user - сумма платежей за определенный период, деленная на количество платящих пользователей когорты.

Обе метрики считаются в определенном окне (количестве дней от инсталла) - обычно 7, 28 или 30 дней. Это необходимо для того, чтобы учесть ситуацию, когда пользователи одной когорты (месячной, например) могли прожить разное количество дней в приложении. Или когда необходимо сравнить разные каналы привлечения, рекламные кампании или группы аб-тестов.

<br>

## LTV {-}
Lifetime value - общая сумма платежей, которые сделает пользователь за всю свою жизнь в приложении. Так как для каждого пользователя обычно сложно предсказать, сколько он проживет, то считается как кумулятивный средний чек когорты - общая накопленная сумма платежей, сделанная к определенному дню от инсталла, деленная на количество пользователей когорты. Кривая LTV/cumARPU сходится к общему значению ARPU по всей выборке за все время жизни когорты. 

LTV, фактически, одна из ключевых метрик, так как график LTV/cumARPU позволяет оценить динамику платежей когорты, сделать какие-то предсказания. Отношение LTV и CPI позволяют оценить эффективность рекламных кампаний. Например, за 90 дней от инсталла платежами пользователей возвращается 40-60% затраченных на привлечение денег. На 270-360 - все затраченные, и дальнейшие платежи составляют чистую прибыль (абстрактный пример, в реальности периоды сильно зависят от продукта). Соответственно, если даже в перспективе значение LTV когорты (сколько заплатит каждый привлеченный пользователь) не превысит CPI (сколько стоило привлечение каждого пользователя) когорты, то такая рекламная кампания убыточна и может быть полезна только для увеличение объема пользователей.

### задача 5 {-}
Посчитать кумулятивное ARPU  30  дня по июньской когорте

Алгоритм
 - посчитать сумму гросса по дням лайфтайма
 - посчитать кумулятивную сумму этого гросса c помощью `cumsum()`
 - поделить кумулятивную сумму на общее количество пользователей когорты
 - нарисовать график
 
```{r}
total_users <- installs[dt >= '2022-06-01' & dt < '2022-07-01', uniqueN(user_pseudo_id)]

payers_june <- payments[install_dt >= '2022-06-01' & install_dt < '2022-07-01']
payers_june <- payers_june[lifetime >= 0 & lifetime <= 30]

arpu_cum <- payers_june[, list(gross = sum(gross)), by = lifetime]
arpu_cum <- arpu_cum[order(lifetime)]
arpu_cum[, gross_cum := cumsum(gross)]

#нужен мердж, если по платформам или еще каким группам считаем
arpu_cum[, arpu_cum := gross_cum / total_users]

plot_ly(arpu_cum, x = ~lifetime, y = ~arpu_cum, type = 'scatter', mode = 'line') %>%
  layout(
    # title = 'Выручка в день',
    title = 'кумулятивное ARPU',
    yaxis = list(rangemode = 'tozero')
  ) %>%
  config(displayModeBar = FALSE)
```
 
 
## Полезные материалы {-}

[What Is a Business Model? 30 Successful Types of Business Models You Need to Know](https://fourweekmba.com/what-is-a-business-model/) Коротко, что такое бизнес-модели, рассматриваются 30 разных моделей. Полезно для понимания, как вообще могут зарабатывать разные продукты.

[Основные метрики мобильных приложений](https://apptractor.ru/measure/user-analytics/osnovnyie-metriki-mobilnyih-prilozheniy.html) Очень обзорный материал от devtodev. Есть неплохой блок по метрикам монетизации.


## Домашнее задание {-}

Домашние занятия для желающих. Если будут вопросы или необходимость получить от меня какие-то комментарии - пишите в личку в slack.

Задание можете выполнять на любом доступном вам языке / среде для статистики.

### level 1 (IATYTD)

Прочитайте конспект, разберите практические занятия. Обновите знания по работе с табличками --- аггрегации (групировки), слияния, создание и модификация колонок.

### level 2 (HNTR)

Постройте график гросса (как в задаче 2) с группировкой по времени жизни пользователей в приложении (как ранее в DAU делали).
Датасет по платежам: https://gitlab.com/hse_mar/mar211f/-/raw/main/data/payments_custom.csv

```{r, echo=FALSE}
payments <- fread('https://gitlab.com/hse_mar/mar211f/-/raw/main/data/payments_custom.csv')
payments[, lifetime := as.numeric(pay_dt - install_dt)]

# размечаем группы
payments[lifetime == 0, lifetime_group := '0. 0 day']
payments[lifetime >= 1 & lifetime <= 7, lifetime_group := '1. 1-7 days']
payments[lifetime >= 8 & lifetime <= 28, lifetime_group := '2. 8-28 days']
payments[lifetime >= 28 & lifetime <= 90, lifetime_group := '3. 28-90 days']
payments[lifetime > 90, lifetime_group := '4. 90+ days']

# создаем отдельную группу для тех, про кого мы не знаем
payments[is.na(lifetime_group), lifetime_group := 'unknown']

# считаем количество уникальных пользователей по дням платежей
daily_gross <- payments[, list(gross_group = sum(gross)), by = list(pay_dt, lifetime_group)]

# так как строки специфично сортируются, конвертируем в фактор и указываем уровни
# dau_stat_groups[, lifetime_group := factor(lifetime_group, 
#                                            levels = c('0. 0 day', '1. 1-7 days', '2. 8-28 days', '3. 28-90 days', '4. 90+ days', 'unknown'))]
daily_gross <- daily_gross[order(pay_dt, lifetime_group)]


# рисуем график area
plot_ly(daily_gross, x = ~pay_dt, y = ~gross_group, color = ~lifetime_group,
        type = 'scatter', mode = 'none', stackgroup = 'one') %>%
  layout(
    title = 'Daily gross по группам пользователей',
    xaxis = list(title = ''),
    yaxis = list(title = '', rangemode = 'tozero')) %>%
  config(displayModeBar = FALSE)
```


### level 3 (HMP)

Посчитайте по каждой платформе конверсию в платящих в день инсталла. Когорта -- пришедшие в июне.
Датасет: https://gitlab.com/hse_mar/mar211f/-/raw/main/data/installs.csv

```{r, echo=FALSE}
installs <- fread('https://gitlab.com/hse_mar/mar211f/-/raw/main/data/installs.csv')

# выделяем сегмент пользователей
payers_june <- payments[install_dt >= '2022-06-01' & install_dt < '2022-07-01']
payers_june <- payers_june[lifetime >= 0 & lifetime <= 30]

conversion_platforms = merge(
  installs[dt >= '2022-06-01' & dt < '2022-07-01', list(total_users = uniqueN(user_pseudo_id)), by = platform],
  payers_june[, list(new_payers_30 = uniqueN(user_pseudo_id)), by = platform],
  by = 'platform', all.x = TRUE
)
conversion_platforms[, conversion_30 := new_payers_30 / total_users]
conversion_platforms
```


<br>

### level 4 (UV)

Постройте по платформам накопительную кривую конверсии по дням от инсталла (по аналогии с накопительным ARPU).

```{r, echo=FALSE, warning=FALSE}
first_payment <- payers_june[, list(lifetime = min(lifetime)), by = list(platform, user_pseudo_id)]

conversion_platforms = merge(
  installs[dt >= '2022-06-01' & dt < '2022-07-01', list(total_users = uniqueN(user_pseudo_id)), by = platform],
  first_payment[, list(new_payers = uniqueN(user_pseudo_id)), by = list(lifetime, platform)],
  by = 'platform', all.x = TRUE
)

conversion_platforms <- conversion_platforms[order(platform, lifetime)]
conversion_platforms[, cum_new_payers := cumsum(new_payers), by = platform]
conversion_platforms[, cum_conversion := cum_new_payers / total_users]
plot_ly(conversion_platforms, x = ~lifetime, y = ~cum_conversion, color = ~platform,
        type = 'scatter', mode = 'lines') %>%
  layout(
    title = 'Кумулятивная конверсия в платящих',
    xaxis = list(title = 'день от инсталла'),
    yaxis = list(title = '', rangemode = 'tozero')) %>%
  config(displayModeBar = FALSE)  
```


### level 5 (N)

Посчитайте по каждой платформе конверсию в платящих в день инсталла, суммарно на 3, 7 и 30 дни. Когорта -- пришедшие в июне.

```{r, echo = FALSE}
conversion_platforms_table <- conversion_platforms[, cum_conversion := round(cum_conversion, 3)]
conversion_platforms_table <- dcast(
  conversion_platforms_table[lifetime %in% c(0, 3, 7, 30)],
  platform + total_users ~ paste('day', lifetime),
  value.var = 'cum_conversion'
)
setcolorder(conversion_platforms_table, c(1, 2, 3, 4, 6, 5))
conversion_platforms_table
```

